//
//  InfoTableViewCell.swift
//  DemoAPI
//
//  Created by Murphy on 2024/4/23.
//

import UIKit

class InfoTableViewCell: UITableViewCell {

    @IBOutlet weak var planId: UILabel!
    
    @IBOutlet weak var planName: UILabel!
    
    @IBOutlet weak var companyName: UILabel!
    
    let planIdText = "計畫編號："
    
    let companyNameText = "廠商名稱："
    
    let planNameText = "計畫名稱："
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(data: CompanyInfo){
        if let planId = data.planId{
            self.planId.text = planIdText + planId
        }
        if let companyName = data.companyName{
            self.companyName.text = companyNameText + companyName
        }
        if let planName = data.planName{
            self.planName.text = planNameText + planName
        }
    }
    
}

//
//  ViewController.swift
//  DemoAPI
//
//  Created by Murphy on 2024/4/22.
//

import UIKit
import Alamofire

class ViewController: UIViewController {


    @IBOutlet weak var infoTableView: UITableView!
    
    var companyInfoData: [CompanyInfo]?
    override func viewDidLoad() {
        super.viewDidLoad()
        getApiData()
        setNib()
        infoTableView.delegate = self
        infoTableView.dataSource = self
    }
    
    func getApiData(){
        let url = "https://soa.tainan.gov.tw/Api/Service/Get/04f51299-f57f-4936-b49f-ecb3b1fc1d48"
        
        AF.request(url, method: .get).responseDecodable(of: Plan.self) { response in
            switch(response.result){
            case .success(let dataList):
                self.companyInfoData = dataList.data
                //讓TableView 馬上reloadData
                self.infoTableView.reloadData()
                
            case .failure(let errorMsg):
                print(errorMsg)
            }
        }
    }
    
    func setNib(){
        let nib = UINib(nibName: "InfoTableViewCell", bundle: nil)
        infoTableView.register(nib, forCellReuseIdentifier: "InfoTableViewCell")
    }
    
    

}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companyInfoData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell") as! InfoTableViewCell
        //把cell點擊特效關掉
        cell.selectionStyle = .none
        if let data = companyInfoData?[indexPath.row]{
            cell.setCell(data: data)
        }
        
        return cell
    }
    
}

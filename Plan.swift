//
//  CompanyInfo.swift
//  DemoAPI
//
//  Created by Murphy on 2024/4/23.
//

import UIKit



class Plan: Codable {
    let contentType: String?
    let isImage: Bool?
    let data: [CompanyInfo]?
    let errorCode: Int?
    let id: String?
    let message: String?
    let success: Bool?

}

class CompanyInfo: Codable{
    let planId: String?
    let companyName: String?
    let planName: String?
    enum CodingKeys: String, CodingKey{
        case planId = "計畫編號"
        case companyName = "廠商名稱"
        case planName = "計畫名稱"
    }
}
